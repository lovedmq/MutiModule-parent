<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!-- dataTable 模板 底部分页框部分 -->
<div class="dataTables_info" role="status" aria-live="polite">
	当前页:第${pageView.currentpage}页 | 总记录数:${pageView.totalrecord}条 |
	每页显示:${pageView.recordPerPage}条 | 总页数:${pageView.totalpage}页</div>

<div class="dataTables_paginate paging_simple_numbers">
	<ul class="pagination" id="myPageJSPPagination">
		<li <c:if test="${pageView.currentpage==1 }">class="paginate_button previous disable"</c:if>
			<c:if test="${pageView.currentpage!=1 }">class="paginate_button previous"</c:if> 
			aria-controls="DataTables_Table_0" 
			tabindex="0"><a href="#" onclick="onclickSearch($('#myPageJSPPagination li.active').val() - 1);">Previous</a></li>
		<c:forEach begin="${pageView.pageindex.startindex}" end="${pageView.pageindex.endindex}" var="wp">
			<c:if test="${pageView.currentpage==wp}">
				<li class="paginate_button active" value="${wp}" aria-controls="DataTables_Table_0"
			tabindex="0"><a href="#" onclick="onclickSearch(${wp});">${wp}</a></li>
			</c:if>
			<c:if test="${pageView.currentpage!=wp}">
				<li class="paginate_button " value="${wp}" aria-controls="DataTables_Table_0"
			tabindex="0"><a href="#" onclick="onclickSearch(${wp});">${wp}</a></li>
			</c:if>
		</c:forEach>
		<li <c:if test="${pageView.currentpage == pageView.totalpage }">class="paginate_button next disable"</c:if>
			<c:if test="${pageView.currentpage != pageView.totalpage }">class="paginate_button next"</c:if> 
			aria-controls="DataTables_Table_0"
			tabindex="0" id="DataTables_Table_0_next"><a href="#" onclick="onclickSearch($('#myPageJSPPagination li.active').val() + 1);">Next</a></li>
	</ul>
</div>
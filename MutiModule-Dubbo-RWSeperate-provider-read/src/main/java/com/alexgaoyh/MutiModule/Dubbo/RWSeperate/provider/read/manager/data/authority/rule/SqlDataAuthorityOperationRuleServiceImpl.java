package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.data.authority.rule;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.rule.read.ISqlDataAuthorityOperationRuleReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRule;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRuleMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationView;

@Service(value = "sqlDataAuthorityOperationRuleService")
public class SqlDataAuthorityOperationRuleServiceImpl implements ISqlDataAuthorityOperationRuleReadService{
	
	@Resource(name = "sqlDataAuthorityOperationRuleMapper")
	private SqlDataAuthorityOperationRuleMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<SqlDataAuthorityOperationRule> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public SqlDataAuthorityOperationRule selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}
	
	@Override
	public MyPageView<SqlDataAuthorityOperationRule> generateMyPageViewVO(Map<Object, Object> map) {
		
		int _totalCount = mapper.selectCountByMap(map);
		
		List<SqlDataAuthorityOperationRule> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<SqlDataAuthorityOperationRule> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
		
	}

	@Override
	public List<SqlDataAuthorityOperationView> selectListWithOperationModelByMap(Map<Object, Object> map) {
		return mapper.selectListWithOperationModelByMap(map);
	}

	@Override
	public MyPageView<SqlDataAuthorityOperationView> generateMyPageViewVOWithModel(Map<Object, Object> map) {
		int _totalCount = mapper.selectCountByMap(map);
		
		List<SqlDataAuthorityOperationView> _list = mapper.selectListWithOperationModelByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<SqlDataAuthorityOperationView> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
	}

	@Override
	public SqlDataAuthorityOperationView selectEntityWithOperationModelById(String id) {
		return mapper.selectEntityWithOperationModelById(id);
	}

}

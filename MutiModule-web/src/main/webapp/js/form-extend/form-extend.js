/**
 * 自定义的表单扩展
 * 其中如果需要使用 resetForm() 方法时，需要了解他依赖 jQuery.form.js 文件
 */
var FormExtend = function() {

	return {
		
		/**
		 * 加载表单数据
		 */
		"formLoadData" : function(data, formId) {
			
			if(data != undefined && data != null && data != "null") {

				$("#" + formId).each(function() {
					//input
					$(this).find("input").each(function() {
						var tagName = $(this).attr('name');
						var tagType = $(this).attr('type');
						var obj = eval("("+data+")");
						if(tagName != undefined && tagType != undefined) {
							$(this).val(obj[tagName]);
						}
					});
				});
				
			}
		},
		
		/**
		 * 重置表单
		 */
		"resetForm" : function(formId){
			$("#" + formId).clearForm();
		}
	};
}();

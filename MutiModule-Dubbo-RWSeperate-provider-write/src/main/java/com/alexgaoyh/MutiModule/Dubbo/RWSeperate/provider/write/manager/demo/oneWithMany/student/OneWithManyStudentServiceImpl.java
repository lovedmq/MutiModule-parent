package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.demo.oneWithMany.student;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.student.write.IOneWithManyStudentWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudentMapper;

@Service(value = "oneWithManyStudentService")
public class OneWithManyStudentServiceImpl implements IOneWithManyStudentWriteService{
	
	@Resource(name = "oneWithManyStudentMapper")
	private OneWithManyStudentMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(OneWithManyStudent record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(OneWithManyStudent record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(OneWithManyStudent record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(OneWithManyStudent record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int deleteStudentListByClassId(String classId) {
		return mapper.deleteStudentListByClassId(classId);
	}

	
}

package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.error;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="error")
public class ErrorController {

	@RequestMapping(value = "/10006")
	public ModelAndView index(ModelAndView model, HttpServletRequest request) {
		model.setViewName("error/10006");
		return model;
	}
}

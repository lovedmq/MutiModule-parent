package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.${packageName};

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.read.I${className}ReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.${packageName}.${className};
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.${packageName}.${className}Mapper;

@Service(value = "${smallClassName}Service")
public class ${className}ServiceImpl implements I${className}ReadService{
	
	@Resource(name = "${smallClassName}Mapper")
	private ${className}Mapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<${className}> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public ${className} selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}
	
	@Override
	public MyPageView<${className}> generateMyPageViewVO(Map<Object, Object> map) {
		
		int _totalCount = mapper.selectCountByMap(map);
		
		List<${className}> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<${className}> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
		
	}

}

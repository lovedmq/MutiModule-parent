package com.MutiModule.Hadoop.demo.excel.mapreduce;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelMapper extends
		Mapper<LongWritable, Text, Text, Text> {

	private static Logger LOG = LoggerFactory.getLogger(ExcelMapper.class);

	public void map(LongWritable key, Text value, Context context)
			throws InterruptedException, IOException {
		String line = value.toString();
		
		context.write(new Text(line), null);
		LOG.info("Map processing finished");
	
	}
}

package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.student.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;

/**
 * OneWithManyStudent 模块 写接口
 * @author alexgaoyh
 *
 */
public interface IOneWithManyStudentWriteService {

    int deleteByPrimaryKey(String id);

    int insert(OneWithManyStudent record);

    int insertSelective(OneWithManyStudent record);

    int updateByPrimaryKeySelective(OneWithManyStudent record);

    int updateByPrimaryKey(OneWithManyStudent record);
    
    /**
     * 根据classId 删除班级下的所有studentList
     * @param classId	班级id
     * @return
     */
    int deleteStudentListByClassId(String classId);
}

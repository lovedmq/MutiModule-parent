package com.MutiModule.common.exception;

/**
 * 业务逻辑操作异常
 * @author alexgaoyh
 *
 */
public class MyLogicException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyLogicException() {
		super();
	}

	public MyLogicException(String msg) {
		super(msg);
	}

	public MyLogicException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public MyLogicException(Throwable cause) {
		super(cause);
	}
}
package com.MutiModule.common.utils.RSA.util;

import java.io.Closeable;

public class CloseableUtil {

	 public static void close(Closeable x) {
	        if (x == null) {
	            return;
	        }
	        try {
	            x.close();
	        } catch (Exception e) {
	            e.getStackTrace();
	    }
	}
}

package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class ParamDataLocation extends BaseEntity implements Serializable {
	/**
	 * 父级ID,所属表字段为param_data_location.PARENT_ID
	 */
	private String parentId;

	/**
	 * 层级,所属表字段为param_data_location.LEVEL
	 */
	private String level;

	/**
	 * 邮政编码,所属表字段为param_data_location.ZIP_CODE
	 */
	private String zipCode;

	/**
	 * 区号,所属表字段为param_data_location.CITY_CODE
	 */
	private String cityCode;

	/**
	 * 行政代码,所属表字段为param_data_location.AREA_CODE
	 */
	private String areaCode;

	/**
	 * 名称,所属表字段为param_data_location.NAME
	 */
	private String name;

	/**
	 * 简称,所属表字段为param_data_location.SHORT_NAME
	 */
	private String shortName;

	/**
	 * 组合名,所属表字段为param_data_location.MERGER_NAME
	 */
	private String mergerName;

	/**
	 * 拼音,所属表字段为param_data_location.PINYIN
	 */
	private String pinyin;

	/**
	 * 经度,所属表字段为param_data_location.LNG
	 */
	private String lng;

	/**
	 * 维度,所属表字段为param_data_location.LAT
	 */
	private String lat;

	private static final long serialVersionUID = 1L;

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getMergerName() {
		return mergerName;
	}

	public void setMergerName(String mergerName) {
		this.mergerName = mergerName;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}
}
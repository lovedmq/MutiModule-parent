package com.MutiModule.logback.moc;

import java.io.IOException;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.MutiModule.common.utils.CookieUtilss;

import ch.qos.logback.classic.ClassicConstants;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.Loader;
import ch.qos.logback.core.util.StatusPrinter;

public class MOCInsertUserOperationServletFilter implements Filter {
	
	public void destroy() {
		// do nothing
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		insertIntoMDC(request);
		try {
			chain.doFilter(request, response);
		} finally {
			clearMDC();
		}
	}

	void insertIntoMDC(ServletRequest request) {

		MDC.put(ClassicConstants.REQUEST_REMOTE_HOST_MDC_KEY,
				request.getRemoteHost());

		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			MDC.put(ClassicConstants.REQUEST_REQUEST_URI,
					httpServletRequest.getRequestURI());
			StringBuffer requestURL = httpServletRequest.getRequestURL();
			if (requestURL != null) {
				MDC.put(ClassicConstants.REQUEST_REQUEST_URL,
						requestURL.toString());
			}
			MDC.put(ClassicConstants.REQUEST_QUERY_STRING,
					httpServletRequest.getQueryString());
			MDC.put(ClassicConstants.REQUEST_USER_AGENT_MDC_KEY,
					httpServletRequest.getHeader("User-Agent"));
			MDC.put(ClassicConstants.REQUEST_X_FORWARDED_FOR,
					httpServletRequest.getHeader("X-Forwarded-For"));
			
			if(CookieUtilss.get(httpServletRequest, "alexgaoyh-admin") != null) {
				MDC.put("alexgaoyh-admin", CookieUtilss.get(httpServletRequest, "alexgaoyh-admin"));
			} else {
				MDC.put("alexgaoyh-admin", "未登录用户");
			}
			
		}

	}

	void clearMDC() {
		MDC.remove(ClassicConstants.REQUEST_REMOTE_HOST_MDC_KEY);
		MDC.remove(ClassicConstants.REQUEST_REQUEST_URI);
		MDC.remove(ClassicConstants.REQUEST_QUERY_STRING);
		// removing possibly inexistent item is OK
		MDC.remove(ClassicConstants.REQUEST_REQUEST_URL);
		MDC.remove(ClassicConstants.REQUEST_USER_AGENT_MDC_KEY);
		MDC.remove(ClassicConstants.REQUEST_X_FORWARDED_FOR);
	}

	public void init(FilterConfig arg0) throws ServletException {
		configureViaXML_File();
	}
	
	static void configureViaXML_File() {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		try {
			JoranConfigurator configurator = new JoranConfigurator();
			configurator.setContext(lc);
			lc.reset();
			URL url = Loader.getResourceBySelfClassLoader("MOCInsertUserOperationServletFilter.xml");
			configurator.doConfigure(url);
		} catch (JoranException je) {
			StatusPrinter.print(lc);
		}
	}
}

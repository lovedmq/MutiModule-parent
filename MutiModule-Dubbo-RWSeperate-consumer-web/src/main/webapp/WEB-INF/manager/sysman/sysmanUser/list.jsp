<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
    <!-- Data Tables -->
    <link href="/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Basic Data Tables example with responsive plugin</h5>
			</div>
			<div class="ibox-content">

				<div class="dataTables_wrapper form-inline">
					<div>
						<label>
							Search:
							<input type="search" name="listSearchName" id="listSearchName" value="${listSearchName }" class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0">
						</label>
					</div>
					<div class="clear"></div>
					<div class="DTTT_container">
						<button class="btn btn-primary" type="button" onclick="onclickSearch($('#myPageJSPPagination li.active').val());">搜索</button>
						<c:forEach var="_iconContains" items="${iconContainsList}" varStatus="vs">
							<c:if test="${_iconContains.name == '添加' }">
								<button class="btn btn-primary" type="submit" onclick="location.href='${context_ }/${moduleName }/add'">添加</button>
							</c:if>
						</c:forEach>
					</div>
					
					<table style="overflow-x: scroll;" class="table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline" >
						<thead>
							<tr role="row">
								<th rowspan="1" colspan="1">编码</th>
								<th rowspan="1" colspan="1">姓名</th>
								<th rowspan="1" colspan="1">创建时间</th>
								<th rowspan="1" colspan="1">删除标识</th>
								<th rowspan="1" colspan="1">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${pageView.records}" var="data" varStatus="loop">
								<tr <c:if test="${loop.index%2 == 0}"> class = "gradeA odd "</c:if><c:if test="${loop.index%2 == 0}"> class = "gradeA even "</c:if> role="row">
									<td class="sorting_1">${data.id }</td>
									<td class="center"><a href="${context_ }/${moduleName }/show/${data.id }">${data.name }</a></td>
									<td class="center">${data.createTime }</td>
									<td class="center">
										<c:forEach var="deleteEach" items="${deleteFlagEnum}" varStatus="status">
											<c:if test="${deleteEach == data.deleteFlag }">
												<c:out value="${data.deleteFlag.name }"></c:out>
											</c:if>
										</c:forEach>
									</td>
									<td class="center">
										<c:forEach var="_iconContains" items="${iconContainsList}" varStatus="vs">
											<c:if test="${_iconContains.name == '修改' }">
												<button class="btn btn-info " type="button" onclick="onclickChange($('#myPageJSPPagination li.active').val(), '${data.id}');"><i class="fa fa-paste"></i>修改</button>
											</c:if>
											<c:if test="${_iconContains.name == '角色' }">
												<button class="btn btn-info " type="button" onclick="onclickRoleChange($('#myPageJSPPagination li.active').val(), '${data.id}');"><i class="fa fa-paste"></i>角色</button>
											</c:if>
										</c:forEach>
									</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr role="row">
								<th rowspan="1" colspan="1">编码</th>
								<th rowspan="1" colspan="1">姓名</th>
								<th rowspan="1" colspan="1">创建时间</th>
								<th rowspan="1" colspan="1">删除标识</th>
								<th rowspan="1" colspan="1">操作</th>
							</tr>
						</tfoot>
					</table>
					
					<c:import url="../../../common/dataTable/myTableLengthJSP.jsp"></c:import>
					<br></br>
					<!-- 将分页JSP包含进来 -->
					<c:import url="../../../common/dataTable/myPageJSP.jsp"></c:import>
				</div>

			</div>
		</div>
	</div>
    
    <!-- Data Tables -->
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="/js/plugins/dataTables/dataTables.responsive.js"></script>
	<script type="text/javascript">
		
		$(document).ready(function() {
	        $('.dataTables-example').dataTable({
	        	responsive: true,
	            "dom": 'T<"clear">lfrtip',
	            paginate: false,
	            info: false,
	            searching: false,
	            bAutoWidth: false,
	            aaSorting: [[ 0, "desc" ]]
	        });
		});
	
		var totalPageVar = '${pageView.totalpage}';	
	
		function onclickSearch(currentPage) {
			if(currentPage <= 1) {
				currentPage = 1;
			}
			if(currentPage >= totalPageVar ) {
				currentPage = totalPageVar;
			}
			StandardPost('${context_}/${moduleName}/list',
				{
					currentPage: currentPage,
					recordPerPage: $('#myTableLengthJspSelect').val(),
					listSearchName: $('#listSearchName').val()
				}
			);
		};
		
		function onclickChange(currentPage, id) {
			if(currentPage <= 1) {
				currentPage = 1;
			}
			if(currentPage >= totalPageVar ) {
				currentPage = totalPageVar;
			}
			StandardPost('${context_}/${moduleName}/edit/' + id,
				{
					currentPage: currentPage,
					recordPerPage: $('#myTableLengthJspSelect').val(),
					listSearchName: $('#listSearchName').val()
				}
			);
		};
		
		function onclickRoleChange(currentPage, id) {
			if(currentPage <= 1) {
				currentPage = 1;
			}
			if(currentPage >= totalPageVar ) {
				currentPage = totalPageVar;
			}
			StandardPost('${context_}/manager/sysman/sysmanUserRoleRel/userRoleRelList/' + id,
				{
					currentPage: currentPage,
					recordPerPage: $('#myTableLengthJspSelect').val(),
					listSearchName: $('#listSearchName').val()
				}
			);
		}
	</script>
</body>
</html>
package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.sysman;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;

public class SysmanUserRoleRelTest {

	private ISysmanRoleReadService readService;
	
	//@Before
    public void prepare() throws Exception  {
    	//可以加载多个配置文件
        String[] springConfigFiles = {"Dubbo.xml","mybatis-spring-config.xml" };

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
		context.setConfigLocations(springConfigFiles);
		context.refresh();

		readService = (ISysmanRoleReadService) context.getBean( "sysmanRoleService" );
        
    }
	
}

<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
	<link href="/css/plugins/ztree/zTreeStyle.css" rel="stylesheet">
	<div class="row">
		<ul id="treeDemo" class="ztree"></ul>
	</div>
	<div class="ibox-content">
		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2" style="margin-left: 45%;">
				<button class="btn btn-white" type="submit"
					onclick="javaScript:history.go(-1);return false;">返回</button>
				<button class="btn btn-primary" type="button" onclick="saveResourceRel();">保存</button>
			</div>
		</div>
	</div>
	<!-- ztree -->
	<script src="/js/plugins/ztree/jquery.ztree.core-3.5.js"></script>
	<script src="/js/plugins/ztree/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript">
		<!--
		var sysmanUserId = ${sysmanUserId};
		var currentPage = ${currentPage};
		var recordPerPage = ${recordPerPage};
		
		var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};

		var zNodes = ${zTreenNodesList};
		
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		});
		
		function saveResourceRel() {
			var arr = new Array(); //数组定义标准形式，不要写成Array arr = new Array();
			var treeObj=$.fn.zTree.getZTreeObj("treeDemo");
            nodes=treeObj.getCheckedNodes(true);
            for(var i=0;i<nodes.length;i++){
            	arr.push(nodes[i].id);
            }
			
			StandardPost('${context_}/${moduleName}/saveRels',
				{
					sysmanRoleIds: arr,
					sysmanUserId : '${sysmanUserId}',
					currentPage : currentPage,
					recordPerPage : recordPerPage
				}
			);
		}
		//-->
	</script>
</html>
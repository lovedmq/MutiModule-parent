maven多模块项目管理，整合spring mybatis，多模块划分： web层，service层， persist层， 其他（capthca验证码）；
已经实现后台管理页面RBAC相关权限控制，整合easyui，完成相关CRUD功能。

#upgrade更新部分：

#20150504
	增加模板类文件：
		persist持久化部分使用mybatis，使用maven-mybatis-generator插件，生成对应的模板文件，并添加分页操作
		service部分，自己书写fpl，使用freemarker生成service业务层代码；
#20150509
	增加针对updateByPrimaryKeySelective操作的缓存更新功能 AOP
#20150523
	persist service层增加RBAC功能权限相关处理，并对结果集进行树形结构转换功能。
#20150225
	增加后台用户登陆页面；
#20150526
	后台登陆页面增加登陆校验功能，下一步对/admin下的所有请求，增加listener监听，避免未登录进行访问；
#20150527
	后台登陆增加filter(登陆与否状态校验)，后台管理页面顶部功能,左边sysmanResource树结构实现，
	RedisClient增加方法（过期时间），重置某个key的过期时间；
	对LoginFilter在登陆状态下重置过期时间，避免登陆后即便操作状态下，缓存也会过期；
	后台管理页面用户是否有操作当前资源的权限判定；
#20150528
	后台登陆页面，验证码和用户登陆信息，修改为使用session机制（原先使用redis，会出现缓存覆盖情况）；
#20150601
	persist层增加逻辑删除sql相关；service层增加逻辑删除相关（通用方法写入模板文件中）
	后台管理页面，逻辑删除功能实现，修复后台用户登陆后，登陆信息session与redis缓存匹配的bug
	以实现RBAC相关单表CRUD,下一步实现rel关系处理
#20150602
	后台管理页面，rel关系处理(sysmanUser-sysmanRole-sysmanResource 部分)，
	jquery左右选择 easyui樹形控件
********************************至此完成RBAC相關權限控制部分***************************************************	

#20150608
	persist层，原先使用形如(如下)所示的代码段来定义一个个的bean，现改为MapperScannerConfigurer(查 找 类 路 径 下 的 映 射 器 并 自 动 将 它 们 创 建 成 MapperFactoryBean)
	<bean id="demoMapper" class="org.mybatis.spring.mapper.MapperFactoryBean">  
        <property name="sqlSessionFactory" ref="sqlSessionFactory" />  
        <property name="mapperInterface" value="com.alexgaoyh.MutiModule.persist.demo.DemoMapper" />  
	</bean>
	
	service层，原先使用入刑（如下）所示的代码段来定义一个个的bean，现改为<context:annotation-config /><context:component-scan base-package="X.X.X" />
	<bean id="demoService" class="com.alexgaoyh.MutiModule.service.demo.impl.DemoServiceImpl">
		<property name="demoMapper" ref="demoMapper" />
	</bean>	  
	
#20150623
	增加webapp项目: 
		MutiModule-ueditorDemo(ueditor部分的demo用例);
		MutiModule-ueditor	  (ueditor部分的js/css/image……部分资源文件)；
	增加quickstart项目：
		MutiModule-uedirotClass（ueditor部分的相关java类文件和添加的servlet部分。ueditor源码类文件部分有部分修改）；
		
	前台有多个web子项目，每个子项目有时候都会依赖于ueditor部分，这样，重复性的东西很多，这样，可以吧这一部分的资源抽离出来（MutiModule-class  MutiModule-ueditor），
	这样的话，java类文件放到MutiModule-class的jar包里面，资源文件js/css/image放到MutiModule-ueditor的war包里面；
	MutiModule-uedirotDemo就是一个测试用例，测试可用。
	
#20150624
	增加 MutiModule-logback 模块，用来统一处理日志记录模块
	使用方法详见  MutiModule-logback/README.md 文件中
	
#20150625
	MutiModule-service层，Redis相关
	SerializablePojoRedisTest 测试类，Redis相关，存入value为序列化之后的Student对象，此后如果Student类属性变更，从缓存中获取value值之后进行反序列化Student对象，验证可用性；

#20150701
	增加MutiModule-upload MutiModule-common 相關
	MutiModule-upload : ajaxfileupload.js 文件上傳相關功能測試完成
	MutiModule-common : 通用方法處理相關
	MutiModule-web : 登陸驗證去除session部分，改為使用cookie部分;	同時去除驗證碼部分	
	
#20150704
	MutiModule-upload　部分。将FileUploadServlet 部分移动到MutiModule-common 
	移除 MutiModule-kindeditorClass 模块，将相关的*.java文件移动到 MutiModule-common
	
	删除 MutiModule-upload 部分，将文件上传部分移动到 MutiModule-kindeditorDemo 内部
	形成 jsUpload.jsp 文件，采用 ajaxfileupload.js 进行文件上传，其中文件上传URL类采用 kindeditor 部分，
	整体 MutiModule-kindeditorDemo 模块中，不管是使用富文本编辑器。还是js进行的文件上传，都统一有一个文件处理类（com.MutiModule.common.kindeditor.servlet.FileUploadServlet）
	对整体文件上传类统一规划
	
#20150706
	GraphicsMagick+im4java 进行图片处理操作
	
#20150709
	增加 DesUtilss 类 ，为 对称加密解密类，其中引入自定义的 BASE64DecoderReplace 类，用来提到(sun.misc.BASE64Decoder类)
	对称加密解密算法，可以用来在CookieUtilss 方法中，对存入的cookie值进行处理，防止客户端改变cookie值进行非法操作。
	
#20150710
	增加 MutiModule-citySelect 模块，此模块为jQuery相关的省市区地址选择框，同时满足键盘输入匹配地址，
	省市区选择采用弹出层效果，废除难看的 select的级联选择框。
	
#20150711
	MutiModule-common 模块：
		扩展  mybatis-generator-maven-plugin 功能，在自动生成代码时添加分页功能：
			com.MutiModule.common.mybatis.plugin.PaginationPlugin mysql 分页扩展插件
				com.MutiModule.common.vo.mybatis.pagination.Page 分页插件依赖的分页类 
				
			com.MutiModule.common.mybatis.plugin.DeleteLogicByIdsPlugin 自定义的扩展插件
				实现增加一个方法，方法名称为 deleteLogicByIds， 在 接口文件和XML的sql文件中，都增加自定义的方法。
				
	MutiModule-perisit 模块
		在使用 mybatis-generator-maven-plugin 进行Demo 表结构对应的文件生成后，完成单元测试功能
		
#20150713
	使用新增的  mybatis-generator-maven-plugin 插件，将之前不符合要求的（关联关系表结构-复合主键， 驼峰式字段命名）进行修复；
	
	mybatis-generator-maven-plugin	用来指定自动生成主键的属性（identity字段或者sequences序列）。如果指定这个元素，MBG在生成insert的SQL映射文件中插入一个<selectKey>元素
		<generatedKey column="id" sqlStatement="Mysql" identity="true" type="post"/>
		
	XmlParserUtilss dom4j 处理xml 通用方法
		
#20150714
	维护XmlParserUtilss 方法
	增加 生成xml 字符串的方法，详见单元测试部分   GenerateXMLTest

#20150715
	XMLUtilss 增加方法，解析xml标签元素
	
#20150717
	设定 MutiModule-web 模块为后台管理模块，前后端模块分离，修改链接请求部分，去除无用的 admin 部分；
	
	整合kindeditor 相关功能到 MutiModule-web 模块，同时区分不同的用户 文件空间，防止不同用户上传的文件资源被其他用户看到并且使用
		fileUpload?contextPath=admin&detailPath=anonymous
		fileManager?contextPath=admin&detailPath=1
			针对文件上传，文件空间  相关的两个servlet部分，进行参数传递：
				contextPath : 用来区分项目上下文，区分不同模块下调用方法，比如传递 admin，表明他是后台模块进行的文件上传功能；
				detailPath: 	在区分不同项目级别模块的路径下之后，需要区分不同用户上传的功能，比如传递  1， 表明他为 id=1 的用户上传的，如果传递  anonymous，表明为匿名用户
			
#20150718	
	修复  首页登录时样式文件出不来：  Resource interpreted as Stylesheet but transferred with MIME type text/html
		增加默认的index.jsp 文件重定向功能
		
#20150721
	web模块LoginFilter文件匹配的url-pattern设定为 /* , 并且设置过滤url(init-param)，防止不必要的资源被过滤。 修复上述bug部分
	
#20150722
	web模块，	http://127.0.0.1:8080/MutiModule-web/demo/page/5
	DemoController.java 类文件，增加分页相关的前台插件，添加 jquery.jqpagination.js 前台分页插件，并进行功能处理，现已完成
	
#20150723
	增加讀取配置文件的類文件，詳見 TestPropertiesUtilss TestPropertiesUtilssTest 兩個文件，
		讀取配置文件，並且將配置文件的內容存在緩存中，只需調用getXXX() 方法即可獲取到配置文件中的值			
			增加sso.properties 文件，將sso單點登錄相關的配置文件讀取功能完成；
			
	增加三個webapp子模塊： ssoAuth; ssoDemo1; ssoDemo2		總管三個模塊，用來完成單點登陸功能，
	
		進行本地測試的時候，需要修改他依賴的common模塊的sso.properties文件的相關配置， 並且將上述三個webapp項目加載到tomcat路徑下
		
		ssoAuth:	單點登陸校驗模塊		現階段登陸檢驗的用戶名密碼一致即可完成登陸
		ssoDemo1:	單點登陸客戶端1	http://127.0.0.1:8080/MutiModule-ssoDemo1/userCenter
		ssoDemo2:	單點登陸客戶端2	http://127.0.0.1:8080/MutiModule-ssoDemo2/userCenter
		
#20180724
	增加 BinaryTest 的单元测试部分，使用二进制数据的每一位，表示不同的用户信息;
		注意这样的话， 保存的用户信息位数是有限制的，Integer有位数限制，不能随意扩展，如果扩展位数过多，可以考虑使用String类型的数字；
		
#20150725
	增加 BaseLocation 	省市区 数据库相关， persist/service 相关已完成 selectByPrimaryKey 相关单元测试，下一部分，在web模块中对省市区进行维护(CRUD)
	
#20150727
	WEB模块完成baseLocation省市区相关的zTree异步加载树结构功能，下一步进行CRUD功能；
		
	persist层，generatorConfig.xml 增加两个参数设置，	beginningDelimiter endingDelimiter 将默认生成的sql.xml文件中默认的  (") 去除；
		减少每次使用代码生成工具生成代码之后，又需要手动更改　*。xml 文件（去除" 标识）
		
#20150728
	修复 beginningDelimiter endingDelimiter 为（"） 时候的部分bug，将这一部分旧代码存在的bug进行修复；
	web模块增加后台的条件筛选功能；
	
#20150730
	增加lucene模塊，需進一步優化。	增加persist层直接针对数据库的相关操作，进行实体类型的相关功能性操作(索引文件生成&&分页高亮搜索查询)
	
#20150801
	增加MutiModule-lucene相关
		LuceneIncreTest.java 文件（增量索引操作）
		LuceneUpdateTest.java 文件（更新索引操作）
		LuceneDeleteTest.java 文件（删除索引操作）
		
	修复MutiModule-persist相关的遗留代码部分：
			
		beginningDelimiter endingDelimiter未设置造成的问题
		addCriterion("\"id\" >=", value, "\"id\"");		 改为 		addCriterion("id >=", value, "id");
		
		增加 DemoTransientListStringTest 单元测试部分，	可以用来保存多个图片路径的json串
			此单元测试主要测试  @Transient 注解，可以保存至数据库字段中的值为json字符串(泛型为List<String>)，之后再取出json数据的时候，将这一部分的json串转换为原始数据格式进行输出；
		增加	DemoTransientEnumTest  单元测试部分，可以用来保存多个enum类型的数据格式，json串
			此单元测试主要测试  @Transient 注解，可以保存至数据库字段中的值为json字符串(泛型为 List<Enum>)，之后再取出json数据的时候，将这一部分的json串转换为原始数据格式进行输出；
		
		
#20150803
	增加 MutiModule-static 模块，此模块下完成相关静态资源的配置处理（js/css/image……）;
		抽离出来一份static静态资源模块，为动静分离做准备；
			单元测试详见： MutiModule-ssoAuth		MutiModule-ssoDemo1		MutiModule-ssoDemo2		MutiModule-static 相关
				可见 MutiModule-ssoAuth/README.md 文件
		
	web层增加 GlobalIntercepter 拦截器，增加对 context_ 部分的槽在，减少jsp页面中过多的代码编写；
	
	persist中generatorConfig.xml代码生成器部分，增加deleteFlag中的字段类型，设为enum类型，并且完成相关单元测试部分；
	
#20150804
	web层后台easyui的CRUD功能，使用新开页面代替dialog部分；详见 web层的DemoController.java 文件；
		下一步进一步完成抽离出来的  easyui_dataGrid_blank_extend.js 文件，完善数据回显和部分共用功能的抽离
		
#20150921
	修改数据库连接池，改为 Druid ，并且增加部分监控功能；  详情看 web 模块的 web.xml 文件 增加的servlet 部分；
		StatViewSerlvet展示出来的监控信息比较敏感，是系统运行的内部情况，如果你需要做访问控制，可以配置allow和deny这两个参数
	
#20150922
	druid persist层   配置防御SQL注入攻击
	druid persist层   数据库密码加密
	
#20150928
	增加CMS模塊功能；
	
#20151022
	MutiModule-common 模块中，增加 分布式主键ID生成策略的功能（twitter/snowflake），并且完成单元测试，详见  IdWorkerTest.testIdWorkerInstance()	方法；
	需要注意的是，不同的分布式环境下，这里的  idWorker.properties 配置文件的参数需要不同，需要强烈注意，否则主键生成策略会出现重复情况；
	
#20151028
	JAVA SPI 测试代码，实现并通过
	
#20151111
	MutiModule-Dubbo-RWSeperator-* 模块的创建，测试Dubbo分布式系统环境的搭建，
		api模块为抽离出来的接口模块，注意读写模块的分离；
		persist模块为数据库持久层，
		service业务层分为 读写 两个module, 从业务层的角度来根据读写接口的不同，划分到不同的模块，这样保证后期维护的过程中，读写接口的分离，同时也便于进行水平扩展（代码层面，数据库层面）
		
#20151112
	Dubbo 分布式系统环境的搭建；
		MutiModule-Dubbo-RWSeperator-* 模块
				
			|----MutiModule-Dubbo-RWSeperator-consumer-web
				|----MutiModule-Dubbo-RWSeperator-api
					|----MutiModule-Dubbo-RWSeperator-persist
					
				&& //
				
			|----MutiModule-Dubbo-RWSeperator-provider-write  MutiModule-Dubbo-RWSeperator-provider-read
				|----MutiModule-Dubbo-RWSeperator-api
					|----MutiModule-Dubbo-RWSeperator-persist
		
		此模块换设计：
			1：	抽离出来api模块（api接口层划分清楚读接口 写接口，为了便于数据库级别的读写分离，从接口层面进行分离）；
			2：	api模块依赖于 persist模块（数据持久层）；
			3：	Dubbo的服务提供者、服务消费者 都依赖于 api模块，同时api模块又依赖于persist模块，这样，persist和api模块都单独抽离出来，便于重用；
			4：	Dubbo的服务提供者，此模块内进行 事务控制 DataSourceTransactionManager，将事务部分控制在服务提供者模块中；
			5:	Dubbo的服务消费者，此模块进行 数据展现，引入接口部分进行数据操作和展示；			
		
		provider模块  添加 Druid的监控功能； 仅仅添加了部分监控功能，根据业务可以进行其他方面的配置；
		
#20151113
	MutiModule-Dubbo-RWSeperate-* 代码生成器功能的编写，减少手动书写重复代码量的功能；
		创建 sysmanUser sysmanRole 的代码，全部使用代码生成器功能，测试通过；
		
#20151125
	MutiModule-Dubbo-RWSeperate-consumer-web 模块
		inspinia+ admin templete 框架使用， 整合登陆和左侧菜单栏（菜单栏最多支持三层结构）
			待续未完~;
	
#20151126
	增加sitemesh 装饰器功能；org.sitemesh。sitemesh   将通用的部分页面展现抽离出来，后期只关注业务相关的展示页面即可；
		下一步增加  AOP 相关，将左侧菜单栏部分的数据层展现抽离出来， 并且后期权限校验相关也可以放置到这个AOP 里面
			下一步增加 BaseController 类，抽离通用的方法，尽量做到只需继承此方法，即可完成简单的CRUD 相关功能；
			
	MutiModule-Dubbo-RWSeperate-consumer-web 模块
		controller层配置 @RequestMapping(value="manager/*") 在 manager 路径下的话， 走sitemesh 配置
		controller层 extends BaseController 的话，将走AOP 的配置：(左侧菜单栏的部分数据封装显示)
			com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.aop.LeftMenuDataAop 
			
#20151130
	列表页功能完成；仅完成列表页数据展现相关；(inspinia模板，列表页面数据展现，翻页为页面刷新)
	
#20151201
	Dubbo-RWSeperate-consumer-web模块：整合inspinia+模板分页功能	
		实现 分页栏部分/每页多少条数据  事件相应功能，条件筛选功能，数据回显相关；
		
#20151210
	websocket 简单实现（依靠spring4.x ）：
		浏览器能够与服务器建立连接，通过已建立的通信信道来发送和接收数据而不需要由HTTP协议引入额外其他的开销来实现。
		
	通过 http://192.168.60.134:8080/MutiModule-WebSocket/start 进行访问,点击connect进行连接，之后输入两个整数，点击 Send to Add 按钮即可向服务器发送请求；
		服务器接收请求，返回结果展示在页面中；
		
	可以理解为某种程度上的长连接，期间不需要HTTP协议造成的额外开销；
	
	****
		PS: 此例中 web.xml 几乎为空，spring在4。x的版本中，看到github中的示例代码中web.xml几乎为空，通过注解方式，减少了xml配置文件的编写；
		本例中便采用这种方式，需要简单了解使用；
		
#2015124
	创建Echarts与WebSocket的关联关系；
		echarts在初始化到页面的过程中，客户端（浏览器）直接与服务端创建长连接，后期服务端主动向客户端发送消息，echart页面则开始刷新；
			Demo:
				http://127.0.0.1:8080/MutiModule-echarts/init 初始化echart页面，页面中展现一个图标信息
				另开一个页面访问 http://127.0.0.1:8080/MutiModule-echarts/send 服务器主动向客户端发送消息，则看到之前init链接对应的页面内容发生了变化；
				
	至此，echarts与WebSocket之间则解决了交互问题，并且可以通过服务端控制客户端页面的展现内容相关；
	
#20151218
	pom.xml文件 
		<groupId>org.apache.maven.plugins</groupId>
		<artifactId>maven-compiler-plugin</artifactId>
		<version>2.5</version>
	修改版本部分；
	
#20151231
	generatorConfig.xml 文件 增加
		<columnOverride column="create_time" property="createTime" jdbcType="TIMESTAMP"/>  	
	配置；
		防止mysql 在使用过程中出现 精度缺失（时分秒）；
			并修复之前的sysmanUser sysmanRole sysmanResource 三张表结构存在的问题；
			修复后注意 mapper.xml 文件中 jdbcType 由 Date 类型  调整为 #{createTime,jdbcType=TIMESTAMP}  
			
#2016010
	comsumer-web模块，list列表展示页面增加如下js css 引入；解决小屏幕窗口自适应问题；兼容手机端相应
	响应式布局，解决列表页面自适应问题；
	
	约定大于配置，约定包结构命名规范；
	
#20160103
	新增 MutiModule-Hadoop 模块，用来简单测试 hadoop相关功能，现阶段完成 次数统计 的简单功能；
	
#20160124
	增加Dubbo-RWSeperate模块： jetty启动的功能部分；
	增加json2.js 插件，用来解决前端json解析功能；
	增加jquery.cookie.js 插件，用来解决前端操作cookie功能；
		前端为了有效使用cookie，创建object对象存放多个cookie值，解决cookie数的限制（同时需要注意cookie大小的限制）
		
#20160226
	comsumer-web模块： 新增 BaseEmptyController 类文件，解决 AOP(LeftMenuDataAop) 时部分方法不需依赖 BaseController 的问题：
		public class BaseController<E extends Serializable> extends BaseEmptyController
		
	consumer-web模块：  增加无此权限的情况，直接不显示左侧菜单栏；

#20160229
	consumer-web模块： 修复bug: sys_sysmanresource表中 parent_ids 字段的处理，修复字段保存不准确的情况；
						     修复bug:	 jquery.metisMenu.js 文件，控制菜单回显展开；
					     	$('#li-' + leftMenuId).addClass("active");
					    	$('#li-nav-second-' + leftMenuId).addClass("active");
					    	$('#ul-nav-second-' + leftMenuId).addClass("collapse in");
					    	$('#ul-nav-third-' + leftMenuId).attr('aria-expanded', true);
					    	$('#ul-nav-third-' + leftMenuId).addClass("collapse in");
					    	$('#li-' + leftMenuId + ' .collapse').removeClass("collapse" );

#20160301
	common模块： 增加 File DataBase 操作，解决使用文件进行资源CRUD 的功能；
		FileDatabaseHelperTest 为单元测试，获取console控制台输入的信息，进行资源CRUD功能；				
		
#20160302
	common模块：	增加一致性Hash 的简单实现；
		com.MutiModule.common.utils.hash.ConsistentHash 类文件；	    	
		
#20160307
	新增 MutiModule-simpleRPC 模块，此模块为简单的RPC功能，解决方案为 动态代理（java.lang.reflect.Proxy）+ socket编程；
		包含单元测试，RpcConsumer RpcProvider 两个类；
		
	consumer-web模块： 登陆页面增加 redirect 功能，解决登陆页面直接redirect到用户关注的页面功能；
							左侧菜单部分数据回显，移除cookie部分的依赖，防止左侧菜单由于更换浏览器造成回显失败；
							此部分处理逻辑在 AOP内部 ，LeftMenuDataAop.java 文件；
							
	common模块：	增加  对象集合转换为 属性结构的转换功能；
		/src/test/java 下 com.MutiModule.common.treeNode 包路径下；
			单元测试详见 ToTreeNodeTest.java 文件；
			
#20160308
	Dubbo-consumer模块：	增加按钮级别的权限控制
		解决方案：
			后台权限控制资源配置增加 按钮级别的权限控制部分，并且在分配角色权限时进行分配；
				注意 页面中的权限资源按钮部分的名称要跟页面的名称保持一致，这样，就可以针对按钮级别进行权限控制；
		Dubbo-persist模块： 数据库文件发生变化，进行更新；
		
	Dubbo-consumer Dubbo-provider-read Dubbo-provider-write 三个模块：
		抽离 配置文件 xml 内部的 
			<dubbo:registry protocol="zookeeper" address="192.168.2.211:2181" />
		部分 到 Dubbo-api 模块中，减少重复代码；一个地方修改即可；
			
#2060310
	Dubbo-consumer 按钮级别的功能权限下，不能再存在按钮级别的功能权限；
	
	SpringMVC UTF-8 中文支持	如果SpringMVC 返回的是一个String字符串，这里会出现中文乱码的情况，可以参考如下代码：
	
		@RequestMapping(value = "/utf8Test", produces = "application/json; charset=utf-8")
		@ResponseBody
		public String utf8Test() {
			Map<String, String> map = new HashMap<String, String>();
			map.put("alexgaoyh", "中国");
			return JSONUtilss.toJSon(map);
		}
	
	如有有一个方法如上，通过浏览器访问，会发现如果存在中文情况的话，会出现中文乱码；
		此时需要注意  produces = "application/json; charset=utf-8"  这一段代码段落；
		
#20160311
	下一步判断 菜单级别 无限极操作是否可以实现；
	
#20160321
	升级 org.json.json 包版本，common模块 增加将 xml 字符串 转换为 json 串 的方法；
	
#20160318
	Dubbo-* 模块： 一对多的CRUD 操作；
		需要注意右侧的这种sql格式，mysql执行是会有问题的：	delete mutidatabase a 
		
#20160319
	增加 CFX 依赖，解决直接调用webservice 的单元测试 	com.MutiModule.common.soap.cfx.CFXWebServiceTest
	
#20160321
	Dubbo-consumer 模块，一对多CRUD 功能增加多方delete处理

#20160322
	Dubbo-consumer	模块： 一对多CRUD 修复多方无值时数据查询失败的bug; 修复for循环时nullpointer异常；
	Dubbo-write		模块： 一对多CRUD ,删除操作时真正删除多方数据bug修复；
	Dubbo-consumer	模块：依赖MutiModule-upload模块，解决图片上传操作，webuploader 功能；
	
#20160413
	Dubbo-persist模块： 表结构标准化：
			sys_sysmanuser  -- sys_sysman_user  采用下划线分词模式
				数据库文件已经提交到 Dubbo-RWSeperate-persist 模块：
	
#20160421
	Dubbo-RWSeperate-* 模块：
			将 sys_*  后台 RABC 相关的表结构都进行处理，移除 Example 相关的表结构，改为自定义的通用方法；
				本地扩展mybatis-generator 插件，自定义插件解决基础功能处理；
				后续有新的业务逻辑，则重新扩展此自定义插件；
				
#20160613
	Dubbo-RWSeperate-* 模块：
		将ID 的字段属性修改为 字符串类型； 将 createTime 的字段属性修改为 字符串类型；
			某种程度上先解决数据库无关性的操作；	需要注意，现阶段为保证时区的问题，数据库中存储的都是 0 时区的时间格式(GMT)，在查询出来数据之后，将数据自动转换为系统所属时区的时间格式；
			GMT 就是格林威治标准时间的英文缩写(Greenwich Mean Time 格林尼治标准时间);
			数据库有修改，脚本查看 persist模块的 doc文件夹；
			
#20160618
	Dubbo-RWS*-* 模块：
		省市区数据信息的 zTree 模块的 大数据异步加载功能实现；
		IP地址转换方式功能实现：  ip到地名的映射库(参考 common模块的 IP2RegionUtilss 类注释文件)
		
#20160622
	Dubbo-RWS*-persist:增加model类的自定义注解实现
		为了后续 数据权限的实现，在生成model类的过程中，增加自定义注解部分代码的生成
		
#20160629
	Dubbo-RWSeperate-provider-read 模块： 
		增加AOP 注解，测试解决 数据权限的问题：
			注意 com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.annotation 包下的类文件；
				一个是针对 method（方法），说明这个方法需要进行数据权限的设定，需要在执行前增加额外的数据参数的增加；
				一个是针对class  (类)  ， 说明这个类     需要增加额外的数据权限的设定，其中className 用来存储真正关联的实体类的绝对路径；
				
		通过使用 AOP 方式，在执行service方法之前，动态把相关配置的数据权限加载到入参参数中；
			使用方式，需要在需要植入数据权限的service实现方法部分加上 DataAuthorityMethodAnnotation 注解； 在此service实现类部分加上 DataAuthorityClassAnnotation 注解（同时加上 className 部分（实体类绝对路径））；
			增加相关的 SqlModelVO 类注释，详见 com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.aspect.test.vo.DictDictionaryVOUtilss 类文件，可以通过 DictDictionaryTest 单元测试部分进行测试检查； 
package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.sysman.sysmanRoleResourceRel;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.read.ISysmanRoleResourceRelReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelMapper;

@Service(value = "sysmanRoleResourceRelService")
public class SysmanRoleResourceRelServiceImpl implements ISysmanRoleResourceRelReadService{
	
	@Resource(name = "sysmanRoleResourceRelMapper")
	private SysmanRoleResourceRelMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<SysmanRoleResourceRelKey> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public MyPageView<SysmanRoleResourceRelKey> generateMyPageViewVO(Map<Object, Object> map) {
		int _totalCount = mapper.selectCountByMap(map);
		
		List<SysmanRoleResourceRelKey> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<SysmanRoleResourceRelKey> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
	}


}

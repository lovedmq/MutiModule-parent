package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.data.authority.group;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.utils.StringUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.group.write.ISqlDataAuthorityOperationGroupWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroup;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroupMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRule;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationGroupWithRuleView;

@Service(value = "sqlDataAuthorityOperationGroupService")
public class SqlDataAuthorityOperationGroupServiceImpl implements ISqlDataAuthorityOperationGroupWriteService{
	
	@Resource(name = "sqlDataAuthorityOperationGroupMapper")
	private SqlDataAuthorityOperationGroupMapper mapper;
	
	@Resource(name = "sqlDataAuthorityOperationGroupRuleRelMapper")
	private SqlDataAuthorityOperationGroupRuleRelMapper groupRuleRelMapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SqlDataAuthorityOperationGroup record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SqlDataAuthorityOperationGroup record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(SqlDataAuthorityOperationGroup record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SqlDataAuthorityOperationGroup record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int insertOperationGroupWithRuleView(SqlDataAuthorityOperationGroupWithRuleView groupWithRuleView) {
		SqlDataAuthorityOperationGroup group = (SqlDataAuthorityOperationGroup)groupWithRuleView;
		mapper.insert(group);
		List<SqlDataAuthorityOperationRule> items = groupWithRuleView.getItems();
		if(items != null && items.size() > 0) {
			for (SqlDataAuthorityOperationRule sqlDataAuthorityOperationRule : items) {
				SqlDataAuthorityOperationGroupRuleRelKey groupRuleRelKey = new SqlDataAuthorityOperationGroupRuleRelKey();
				groupRuleRelKey.setSysDataAuthorityGroupId(group.getId());
				groupRuleRelKey.setSysDataAuthorityRuleId(sqlDataAuthorityOperationRule.getId());
				groupRuleRelMapper.insert(groupRuleRelKey);
			}
		}
		return items.size();
	}

	@Override
	public int insertOperationGroupWithRuleView(SqlDataAuthorityOperationGroup record, String[] ruleIds) {
		mapper.insert(record);
		if(null != ruleIds && ruleIds.length > 0) {
			for (int i = 0; i < ruleIds.length; i++) {
				SqlDataAuthorityOperationGroupRuleRelKey groupRuleRelKey = new SqlDataAuthorityOperationGroupRuleRelKey();
				groupRuleRelKey.setSysDataAuthorityGroupId(record.getId());
				groupRuleRelKey.setSysDataAuthorityRuleId(ruleIds[i]);
				groupRuleRelMapper.insert(groupRuleRelKey);
			}
			return ruleIds.length;
		} else {
			return 0;
		}
	}
	
}

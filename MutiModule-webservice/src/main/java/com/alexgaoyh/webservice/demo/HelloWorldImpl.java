package com.alexgaoyh.webservice.demo;

public class HelloWorldImpl implements IHelloWorld {
	
	@Override
	public String sayHello(String name) {
		return name + " sayHello: Hello World!";
	}
	
	@Override
	public String sayHi() {
		return "sayHi: hello world";
	}

	@Override
	public String say2Params(String name, String content) {
		return name + " sayHi: " + content;
	}
	
}
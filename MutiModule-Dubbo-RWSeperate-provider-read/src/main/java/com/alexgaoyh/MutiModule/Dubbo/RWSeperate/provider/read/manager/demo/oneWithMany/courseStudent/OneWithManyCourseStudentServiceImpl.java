package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.demo.oneWithMany.courseStudent;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.courseStudent.read.IOneWithManyCourseStudentReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentMapper;

@Service(value = "oneWithManyCourseStudentService")
public class OneWithManyCourseStudentServiceImpl implements IOneWithManyCourseStudentReadService{
	
	@Resource(name = "oneWithManyCourseStudentMapper")
	private OneWithManyCourseStudentMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<OneWithManyCourseStudentKey> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public MyPageView<OneWithManyCourseStudentKey> generateMyPageViewVO(Map<Object, Object> map) {
		int _totalCount = mapper.selectCountByMap(map);
		
		List<OneWithManyCourseStudentKey> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<OneWithManyCourseStudentKey> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
	}

	@Override
	public int deleteByMap(Map<Object, Object> map) {
		return mapper.deleteByMap(map);
	}


}

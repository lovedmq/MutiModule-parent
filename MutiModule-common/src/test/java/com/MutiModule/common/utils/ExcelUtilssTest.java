package com.MutiModule.common.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Test;

public class ExcelUtilssTest {

	//@Test
	public void readExcelTitleTest() {
        try {
			String[] str = ExcelReader.readExcelTitle("d://1.xls");
			for (int i = 0; i < str.length; i++) {
				System.out.println(str[i]);
			}
			String[] str1 = ExcelReader.readExcelTitle("d://1.xlsx");
			for (int i = 0; i < str1.length; i++) {
				System.out.println(str1[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void readExcelContentTest() {
        try {
        	Set<String>  set = new HashSet<String>();
        	Sheet sheet = ExcelReader.readExcelSheet("d://1.xls", 1);
        	int rowNum = sheet.getLastRowNum();
        	for (int i = 1; i <= rowNum; i++) {
        		Row row = sheet.getRow(i);
        		String _str = ExcelReader.getCellFormatValue(row.getCell((short) 4)).trim() + ExcelReader.getCellFormatValue(row.getCell((short) 6)).trim();
        		set.add(_str);
        	}
        	System.out.println(set.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private int getLengthTest(String filePath) {
		Set<String>  set = new HashSet<String>();
    	Sheet sheet = ExcelReader.readExcelSheet(filePath, 1);
    	int rowNum = sheet.getLastRowNum();
    	for (int i = 1; i <= rowNum; i++) {
    		Row row = sheet.getRow(i);
    		String _str = ExcelReader.getCellFormatValue(row.getCell((short) 4)).trim() + ExcelReader.getCellFormatValue(row.getCell((short) 6)).trim();
    		set.add(_str);
    	}
    	return set.size();
	}
	
	/**
	 *  单日不重复的订单数  -----
	 *  TODO
	 */
	//@Test
	public void readFileFolderList() {
		String filePath = "C:\\Users\\lenovo\\Desktop\\12.22订单汇总\\12.22订单汇总";
		
		List<String> list = FileUtilss.listFileNames(filePath);
		for(String _str : list) {
			System.out.println(_str  + "  :" + getLengthTest(filePath + File.separator + _str));
		}
	}
	
	private Set<String> getFileSet(String filePath , Set<String>  set) {
    	Sheet sheet = ExcelReader.readExcelSheet(filePath, 1);
    	int rowNum = sheet.getLastRowNum();
    	for (int i = 1; i <= rowNum; i++) {
    		Row row = sheet.getRow(i);
    		String _str = ExcelReader.getCellFormatValue(row.getCell((short) 4)).trim() + ExcelReader.getCellFormatValue(row.getCell((short) 6)).trim();
    		set.add(_str);
    	}
    	return set;
	}
	
	private List<String> getFileList(String filePath , List<String>  list) {
    	Sheet sheet = ExcelReader.readExcelSheet(filePath, 1);
    	int rowNum = sheet.getLastRowNum();
    	for (int i = 1; i <= rowNum; i++) {
    		Row row = sheet.getRow(i);
    		String _str = ExcelReader.getCellFormatValue(row.getCell((short) 4)).trim() + ExcelReader.getCellFormatValue(row.getCell((short) 6)).trim();
    		list.add(_str);
    	}
    	return list;
	}
	
	/**
	 * 连续两日交易客户数
	 */
	//@Test
	public void sdfds() {
		String filePath1 = "C:\\Users\\lenovo\\Desktop\\12.21订单汇总\\12.21订单汇总";
		String filePath2 = "C:\\Users\\lenovo\\Desktop\\12.22订单汇总\\12.22订单汇总";
		List<String> list1 = FileUtilss.listFileNames(filePath1);
		List<String> list2 = FileUtilss.listFileNames(filePath2);
		for(String _str1 : list1) {
			for(String _str2 : list2) {
				if(StringUtilss.getHanZi(_str1).equals(StringUtilss.getHanZi(_str2))) {
					Set<String>  set = new HashSet<String>();
					set = getFileSet(filePath1 + File.separator + _str1, set);

					Set<String>  set1 = new HashSet<String>();
					set1 = getFileSet(filePath2 + File.separator + _str2, set1);
					
					Set<String> _tmp = new HashSet<String>();
					_tmp.addAll(set);
					_tmp.addAll(set1);
					
					
					System.out.println(_str2 + "         " + (set.size() + set1.size() - _tmp.size()));
				}
			}
		}
	}
	
}

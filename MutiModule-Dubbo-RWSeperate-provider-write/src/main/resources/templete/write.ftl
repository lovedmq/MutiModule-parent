package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.${packageName};

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.write.I${className}WriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.${packageName}.${className};
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.${packageName}.${className}Mapper;

@Service(value = "${smallClassName}Service")
public class ${className}ServiceImpl implements I${className}WriteService{
	
	@Resource(name = "${smallClassName}Mapper")
	private ${className}Mapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(${className} record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(${className} record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(${className} record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(${className} record) {
		return mapper.updateByPrimaryKey(record);
	}
	
}

package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.param.data.location;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.MutiModule.common.vo.zTree.ZTreeNodesBigDataAsync;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.read.IParamDataLocationReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location.ParamDataLocation;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location.ParamDataLocationMapper;

@Service(value = "paramDataLocationService")
public class ParamDataLocationServiceImpl implements IParamDataLocationReadService{
	
	@Resource(name = "paramDataLocationMapper")
	private ParamDataLocationMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<ParamDataLocation> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public ParamDataLocation selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}
	
	@Override
	public MyPageView<ParamDataLocation> generateMyPageViewVO(Map<Object, Object> map) {
		
		int _totalCount = mapper.selectCountByMap(map);
		
		List<ParamDataLocation> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<ParamDataLocation> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
		
	}

	@Override
	public List<ParamDataLocation> selectTopLocationList() {
		return mapper.selectTopLocationList();
	}

	@Override
	public List<ParamDataLocation> selectTopLocationListByParentId(String parentId) {
		return mapper.selectTopLocationListByParentId(parentId);
	}

	@Override
	public List<ZTreeNodesBigDataAsync> selectSingleZTreeNodeListByParentId(String parentId) {
		return mapper.selectSingleZTreeNodeListByParentId(parentId);
	}

}

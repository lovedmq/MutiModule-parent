package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.item.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;

/**
 * DictDictionaryItem 模块 写接口
 * @author alexgaoyh
 *
 */
public interface IDictDictionaryItemWriteService {

	int deleteByPrimaryKey(String id);

	int insert(DictDictionaryItem record);

	int insertSelective(DictDictionaryItem record);

	int updateByPrimaryKeySelective(DictDictionaryItem record);

	int updateByPrimaryKey(DictDictionaryItem record);
	
	/**
     * 根据外键关联，将字典值部分数据全部删除
     * @param dictId
     * @return
     */
    int deleteByDictid(String dictId);
}

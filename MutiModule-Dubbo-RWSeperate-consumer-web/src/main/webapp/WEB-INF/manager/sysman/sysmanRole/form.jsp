<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/css/plugins/ztree/zTreeStyle.css" rel="stylesheet">
	
<input type="hidden" id="id" name="id" value="${entity.id }"></input>
<input type="hidden" id="parentId" name="parentId" value="${entity.parentId }"></input>
<input type="hidden" id="createTime" name="createTime" value="${entity.createTime }"></input>
<input type="hidden" id="deleteFlag" name="deleteFlag" value="${entity.deleteFlag }"></input>
<div class="form-group">
	<label class="col-sm-2 control-label">资源名</label>
	<div class="col-sm-10">
		<input type="text" id="name" name="name" value="${entity.name }"
			class="form-control">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">描述</label>
	<div class="col-sm-10">
		<input type="text" id="description" name="description" value="${entity.description }"
			class="form-control">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">父节点</label>
	<div class="col-sm-10">
		<input type="text" id="parentName" name="parentName" value="" onclick="showMenu();"
			class="form-control">
		<div id="menuContent" class="menuContent" style="display:none;">
			<ul id="treeDemo" class="ztree" style="margin-top:0; width:180px;"></ul>
		</div>
	</div>
</div>

<!-- ztree -->
<script src="/js/plugins/ztree/jquery.ztree.core-3.5.js"></script>
<script src="/js/plugins/ztree/jquery.ztree.excheck-3.5.js"></script>

<script type="text/javascript">
	<!--
	var setting = {
		check: {
			enable: true,
			chkStyle: "radio",
			radioType: "all"
		},
		view: {
			dblClickExpand: false
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			onClick: onClick,
			onCheck: onCheck
		}
	};

	var zNodes =${zTreeListJSON};

	function onClick(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("treeDemo");
		zTree.checkNode(treeNode, !treeNode.checked, null, true);
		return false;
	}

	function onCheck(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("treeDemo");
		var nodes = zTree.getCheckedNodes(true);
		if(nodes.length != 0) {
			for (var i=0, l=zNodes.length; i<l; i++) {
				if(zNodes[i].id == nodes[0].id) {
					$("#parentName").val(zNodes[i].name);
					$("#parentId").val(zNodes[i].id);
				}
			}
		} else {
			$("#parentName").val('');
			$("#parentId").val('-1');
		}
		
	}

	function showMenu() {
		var parentIdObj = $("#parentName");
		var cityOffset = $("#parentName").offset();
		$("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + parentIdObj.outerHeight() + "px"}).slideDown("fast");

		$("body").bind("mousedown", onBodyDown);
	}
	function hideMenu() {
		$("#menuContent").fadeOut("fast");
		$("body").unbind("mousedown", onBodyDown);
	}
	function onBodyDown(event) {
		if (!(event.target.id == "menuBtn" || event.target.id == "parentName" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
			hideMenu();
		}
	}
	//-->
</script>
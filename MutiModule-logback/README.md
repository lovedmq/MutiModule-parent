此模块为logback的日志模块，用来记录相关日志信息
	具体日志信息记录方式在  logback.xml 文件中
	APPTest方法中，有测试用例，记录相关的日志信息

使用此模块的方式为：
	pom.xml 添加此模块的依赖关系
		<dependency>
			<groupId>com.alexgaoyh</groupId>
			<artifactId>MutiModule-logback</artifactId>
			<version>${project.version}</version>
		</dependency>
		
	类文件中增加 
		Logger logger = LoggerFactory.getLogger(XXXXXX.class);
		
		方法中使用如下方法来增加日志记录的信息
			logger.debug("XXXXXX");
			logger.info("XXXXXX");
			logger.warn("XXXXXX");
			logger.error("XXXXXX");
			
#20151110
	
	-------------------------------------------------------------------------------------------------------------------------------------------------
		Logback – different log file for each thread
		
		logback.xml 文件中新增如下代码，之后创建 Head.java 类文件，单元测试文件为 HeadTest.java 文件
		
			<appender name="FILE-THREAD" class="ch.qos.logback.classic.sift.SiftingAppender">
				<!-- This is MDC value -->
				<!-- We will assign a value to 'logFileName' via Java code -->
				<discriminator>
					<key>logFileName</key>
					<defaultValue>head0</defaultValue>
				</discriminator>
				<sift>
					<!-- A standard RollingFileAppender, the log file is based on 'logFileName' 
						at runtime -->
					<appender name="FILE-${logFileName}"
						class="ch.qos.logback.core.rolling.RollingFileAppender">
						<file>${LOG_PATH}/${logFileName}.log</file>
						<encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
							<Pattern>
								%d{yyyy-MM-dd HH:mm:ss} %mdc [%thread] %level %logger{35} - %msg%n
							</Pattern>
						</encoder>
						<rollingPolicy
							class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
							<FileNamePattern>${LOG_PATH}/${logFileName}.%i.log.zip
							</FileNamePattern>
							<MinIndex>1</MinIndex>
							<MaxIndex>10</MaxIndex>
						</rollingPolicy>
						<triggeringPolicy
							class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
							<MaxFileSize>10MB</MaxFileSize>
						</triggeringPolicy>
					</appender>
				</sift>
			</appender>
			
			<logger name="com.MutiModule.logback" level="debug" additivity="false">
				<appender-ref ref="FILE-THREAD" />
			</logger>
		
	-------------------------------------------------------------------------------------------------------------------------------------------------
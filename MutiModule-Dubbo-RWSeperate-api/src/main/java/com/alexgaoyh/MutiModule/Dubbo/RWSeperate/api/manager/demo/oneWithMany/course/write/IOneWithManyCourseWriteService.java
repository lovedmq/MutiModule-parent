package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourse;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;

/**
 * OneWithManyCourse 模块 写接口
 * @author alexgaoyh
 *
 */
public interface IOneWithManyCourseWriteService {

	int deleteByPrimaryKey(String id);

	int insert(OneWithManyCourse record);

	int insertSelective(OneWithManyCourse record);

	int updateByPrimaryKeySelective(OneWithManyCourse record);

	int updateByPrimaryKey(OneWithManyCourse record);
	
	// alexgaoyh add
    /**
     * 添加 匹配存入 班级学生 两种关联关系的处理
     * @param view	班级学生的集合
     * @return
     */
	String insertCourseStudentView(CourseStudentView view);
    
    /**
     * 更新 匹配更新 班级学生 两种关联关系的处理
     * @param view	班级学生的集合
     * @return
     */
	String updateCourseStudentView(CourseStudentView view);
}

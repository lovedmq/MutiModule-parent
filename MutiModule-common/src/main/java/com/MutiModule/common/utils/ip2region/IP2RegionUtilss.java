package com.MutiModule.common.utils.ip2region;

import java.io.IOException;

import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.utils.ip2region.source.DataBlock;
import com.MutiModule.common.utils.ip2region.source.DbConfig;
import com.MutiModule.common.utils.ip2region.source.DbMakerConfigException;
import com.MutiModule.common.utils.ip2region.source.DbSearcher;
import com.MutiModule.common.utils.ip2region.source.Util;

/**
 * 注意，此类不支持打包后直接调用，原因在于不能读取jar包内的文件并将其转换为File类（第三方提供的方法中仅支持这种方式）
 * 故如果在本模块中使用此方法，参考main方法，
 * 如果将此模块打包为jar包后供其他方法使用的话，需要将 resources\ip2region\ip2region.db 这个文件拷贝到自己的模块中，
 * 并将这个文件路径传递到此util方法中即可。
 * @author alexgaoyh
 *
 */
public class IP2RegionUtilss {
	
	public static String ip2RegionMethod(String inputIpAddr) {
		return ip2RegionMethod(inputIpAddr, null);
	}

	public static String ip2RegionMethod(String inputIpAddr, String dbFilePath) {
		if(StringUtilss.isEmpty(dbFilePath)) {
			dbFilePath = IP2RegionUtilss.class.getResource("/ip2region/ip2region.db").getPath();
		}
    	if( Util.isIpAddress(inputIpAddr) == false) {
    		return "IP地址校验错误";
    	} else {
			try {
				DbConfig config = new DbConfig();
				DbSearcher seacher = new DbSearcher(config, dbFilePath);
				DataBlock fdata = seacher.binarySearch(inputIpAddr);
				return fdata.getRegion();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (DbMakerConfigException e) {
				e.printStackTrace();
			}
			return "IP地址解析错误";
    	}
	}
	
	public static void main(String[] args) {
		String region = ip2RegionMethod("61.163.96.94");
		System.out.println(region);
	}
}

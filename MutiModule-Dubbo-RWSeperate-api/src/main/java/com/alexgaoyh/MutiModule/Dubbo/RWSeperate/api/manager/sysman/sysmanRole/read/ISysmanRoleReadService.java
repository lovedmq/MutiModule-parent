package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;

/**
 * SysmanRole 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISysmanRoleReadService {

	int selectCountByMap(Map<Object, Object> map);

    List<SysmanRole> selectListByMap(Map<Object, Object> map);

    SysmanRole selectByPrimaryKey(String id);

 // alexgaoyh add begin
 	List<TreeNode> selectTreeNodeBySysmanRoleId(String id);

 	List<ZTreeNodes> selectAllResourceZTreeNode();

 	List<SysmanRole> selectRoleListByUserId(String userId);

 	/**
 	 * 获取到所有 parent_id 为 null 的 资源集合
 	 * 
 	 * @return
 	 */
 	List<SysmanRole> selectSysmanRoleListByNullParentId();

 	/**
 	 * 根据当前角色 ID 与 当前资源的主键 id，获取到 用户包含的 权限集合
 	 * 
 	 * @param roleId
 	 * @param parentId
 	 * @return
 	 */
 	List<SysmanRole> selectIconRoleByRoleIdAndParentId(@Param("roleId") String roleId,
 			@Param("parentId") String parentId);
 	
 	/**
      * 获取分页实体信息部分
      * @param map
      * @return
      */
     MyPageView<SysmanRole> generateMyPageViewVO(Map<Object, Object> map);
     
     List<String> selectParentIdsByPrimaryKey(String id, List<String> list);
     
     List<TreeNode> selectTreeNodeListByUserId(String userId);
    
}

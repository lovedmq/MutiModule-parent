<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML" action="${context_ }/${moduleName }/doSaveView">
					<input type="hidden" id="id" name="id" value="${entity.id }"></input>
					<div class="form-group">
						<label class="col-sm-2 control-label">班级名称</label>
						<div class="col-sm-10">
							<input type="text" id="name" name="name" value="${entity.name }"
								class="form-control">
						</div>
					</div>				
					<div class="hr-line-dashed"></div>
					<div class="row">
			            <div class="col-lg-12">
			            	<div class="ibox float-e-margins">
			            		<div class="ibox-content">
						            <div class="">
						            <a onclick="fnClickAddRow();" href="javascript:void(0);" class="btn btn-primary ">Add a new row</a>
						            </div>
						            <table class="table table-striped table-bordered table-hover " id="editable" >
							            <thead>
							            <tr>
							                <th>学生姓名</th>
							                <th>删除</th>
							            </tr>
							            </thead>
							            <tbody>
							            </tbody>
							            <tfoot>
							            <tr>
							                <th>学生姓名</th>
							                <th>删除</th>
							            </tr>
							            </tfoot>
            						</table>
          						</div>
				            </div>
			            </div>
			        </div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);">返回</button>
							<button class="btn btn-primary" type="submit">保存</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Data Tables -->
	<script src="/js/plugins/jeditable/jquery.jeditable.js"></script>
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
	
	<!-- jquery validate scripts -->
	<script src="/js/plugins/validate/jquery.validate.js"></script>
	<script src="/js/plugins/validate/additional-methods.js"></script>
	<script src="/js/${moduleName }/form-validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/* Init DataTables */
            var oTable = $('#editable').dataTable({
                "bServerSide": false, 
                'bPaginate': false, //是否分页 
                "bProcessing": false, //datatable获取数据时候是否显示正在处理提示信息。 
                "iDisplayLength": 10, //每页显示10条记录 
                'bFilter': false, //是否使用内置的过滤功能 
                "bInfo" : false //是否显示页脚信息，DataTables插件左下角显示记录数  
            });

		});
		
		function fnClickAddRow() {
			var _rowNumber = 0;
			$('#editable').find('tbody tr').each(function() {
				if($(this).attr("role")!=undefined) {
					_rowNumber++;
				}
			});
            $('#editable').dataTable().fnAddData( [
                "<input type='text' id='studentList["+_rowNumber+"].name' name='studentList["+_rowNumber+"].name' value=''>",
                "<button class='btn btn-info ' type='button' onclick='delThisInfo(this);'><i class='fa fa-paste'></i>删除</button>" ] );

        }
	</script>
</body>
</html>
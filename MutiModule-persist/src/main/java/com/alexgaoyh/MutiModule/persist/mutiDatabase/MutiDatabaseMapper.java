package com.alexgaoyh.MutiModule.persist.mutiDatabase;

import com.alexgaoyh.MutiModule.persist.mutiDatabase.MutiDatabase;
import com.alexgaoyh.MutiModule.persist.mutiDatabase.MutiDatabaseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MutiDatabaseMapper {
    int countByExample(MutiDatabaseExample example);

    int deleteByExample(MutiDatabaseExample example);

    int insert(MutiDatabase record);

    int insertSelective(MutiDatabase record);

    List<MutiDatabase> selectByExample(MutiDatabaseExample example);

    int updateByExampleSelective(@Param("record") MutiDatabase record, @Param("example") MutiDatabaseExample example);

    int updateByExample(@Param("record") MutiDatabase record, @Param("example") MutiDatabaseExample example);
}
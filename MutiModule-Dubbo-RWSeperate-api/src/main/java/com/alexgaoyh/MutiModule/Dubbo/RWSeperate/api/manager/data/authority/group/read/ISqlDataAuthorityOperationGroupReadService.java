package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.group.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityCurrSysUseLoginAnnotation;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroup;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationGroupWithRuleView;

/**
 * SqlDataAuthorityOperationGroup 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationGroupReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<SqlDataAuthorityOperationGroup> selectListByMap(Map<Object, Object> map);

	SqlDataAuthorityOperationGroup selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<SqlDataAuthorityOperationGroup> generateMyPageViewVO(Map<Object, Object> map);
    
    /**
     * 获取分页实体信息部分
     * @param currentSysmanUserLoginId	允许为空，为空的话则调用单参数的方法
     * 		当前系统登陆用户ID，用来获取当前系统登陆用户的信息--数据权限控制会使用
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<SqlDataAuthorityOperationGroup> generateMyPageViewVO(@DataAuthorityCurrSysUseLoginAnnotation(value = "currentSysmanUserLoginId") String currentSysmanUserLoginId, Map<Object, Object> map);

    
    // alexgaoyh
    
    /**
     * 根据主键ID，查询出 数据权限组 的相关信息，包含 数据权限规则部分
     * @param id	主键ID
     * @return
     */
    SqlDataAuthorityOperationGroupWithRuleView selectGroupWithRuleByPrimaryKey(String id);
    
}

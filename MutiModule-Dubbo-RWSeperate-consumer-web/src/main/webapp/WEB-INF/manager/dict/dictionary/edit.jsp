<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<link href="/css/plugins/ztree/zTreeStyle.css" rel="stylesheet">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML">
					<input type="hidden" id="id" name="id" value="${dictDictionaryWithItemView.id }"></input>
					<input type="hidden" id="deleteFlag" name="deleteFlag" value="${dictDictionaryWithItemView.deleteFlag }"></input>
					<input type="hidden" id="createTime" name="createTime" value="${dictDictionaryWithItemView.createTime }"></input>
					<input type="hidden" id="currentPage" name="currentPage" value="${currentPage }"></input>
					<input type="hidden" id="recordPerPage" name="recordPerPage" value="${recordPerPage }"></input>
					<div class="form-group">
						<label class="col-sm-2 control-label">字典名称</label>
						<div class="col-sm-10">
							<input type="text" id="content" name="content" value="${dictDictionaryWithItemView.content }"
								class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">字典前缀</label>
						<div class="col-sm-10">
							<input type="text" id="perfixCode" name="perfixCode" value="${dictDictionaryWithItemView.perfixCode }"
								class="form-control">
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 control-label">字典类型</label>
						<div class="col-sm-10">
							<input type="text" id="type" name="type" value="${dictDictionaryWithItemView.type }"
								class="form-control">
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 control-label">字典值</label>
						[ <a id="addParent" href="#" title="增加父节点" onclick="return false;">增加父节点</a> ]
						&nbsp;&nbsp;&nbsp;&nbsp;
						[ <a id="addLeaf" href="#" title="增加叶子节点" onclick="return false;">增加叶子节点</a> ]
						&nbsp;&nbsp;&nbsp;&nbsp;
						[ <a id="edit" href="#" title="编辑名称" onclick="return false;">编辑名称</a> ]
						&nbsp;&nbsp;&nbsp;&nbsp;
						[ <a id="remove" href="#" title="删除节点" onclick="return false;">删除节点</a> ]
						&nbsp;&nbsp;&nbsp;&nbsp;
						[ <a id="clearChildren" href="#" title="清空子节点" onclick="return false;">清空子节点</a> ]
						&nbsp;&nbsp;&nbsp;&nbsp;
						[ <a id="clearSelectedNode" href="#" title="清除选中节点" onclick="return false;">清除选中节点</a> ]
						&nbsp;&nbsp;&nbsp;&nbsp;
						<!-- [ <a id="getAllNode" href="#" title="获取所有节点" onclick="return false;">获取所有节点</a> ] -->
						<div class="col-sm-10">
							<ul id="treeDemo" class="ztree"></ul>
						</div>
					</div>				
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);">返回</button>
							<button class="btn btn-primary" type="button" id="submitButton" onclick="submitButtonUpdate()">保存</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- ztree -->
	<script src="/js/plugins/ztree/jquery.ztree.core-3.5.js"></script>
	<script src="/js/plugins/ztree/jquery.ztree.excheck.js"></script>
	<script src="/js/plugins/ztree/jquery.ztree.exedit.js"></script>
	
	<!-- jquery validate scripts -->
	<script src="/js/plugins/validate/jquery.validate.js"></script>
	<script src="/js/plugins/validate/additional-methods.js"></script>
	<script src="/js/${moduleName }/form-validate.js"></script>
	<script type="text/javascript">
		<!--
		var setting = {
			view: {
				selectedMulti: false
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				keep: {
					parent:true,
					leaf:true
				},
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeRemove: beforeRemove,
				beforeRename: beforeRename,
				onRemove: onRemove
			}
		};
	
		var zNodes = ${zNodes};
		var className = "dark";
		function beforeDrag(treeId, treeNodes) {
			return false;
		}
		function beforeRemove(treeId, treeNode) {
			className = (className === "dark" ? "":"dark");
			return confirm("确认删除 节点 -- " + treeNode.name + " 吗？");
		}
		function onRemove(e, treeId, treeNode) {
		}
		function beforeRename(treeId, treeNode, newName) {
			if (newName.length == 0) {
				alert("节点名称不能为空.");
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				setTimeout(function(){zTree.editName(treeNode)}, 10);
				return false;
			}
			return true;
		}
	
		var newCount = ${maxExistingZTreeNodesNum};
		function add(e) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			isParent = e.data.isParent,
			nodes = zTree.getSelectedNodes(),
			treeNode = nodes[0];
			if (treeNode) {
				treeNode = zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, isParent:isParent, name:"new node" + (newCount++)});
			} else {
				treeNode = zTree.addNodes(null, {id:(100 + newCount), pId:0, isParent:isParent, name:"new node" + (newCount++)});
			}
			if (treeNode) {
				zTree.editName(treeNode[0]);
			} else {
				alert("叶子节点被锁定，无法增加子节点");
			}
		};
		function edit() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getSelectedNodes(),
			treeNode = nodes[0];
			if (nodes.length == 0) {
				alert("请先选择一个节点");
				return;
			}
			zTree.editName(treeNode);
		};
		function remove(e) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getSelectedNodes(),
			treeNode = nodes[0];
			if (nodes.length == 0) {
				alert("请先选择一个节点");
				return;
			}
			var callbackFlag = $("#callbackTrigger").attr("checked");
			zTree.removeNode(treeNode, callbackFlag);
		};
		function clearChildren(e) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getSelectedNodes(),
			treeNode = nodes[0];
			if (nodes.length == 0 || !nodes[0].isParent) {
				alert("请先选择一个父节点");
				return;
			}
			zTree.removeChildNodes(treeNode);
		};
		function clearSelectedNode(e) {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getSelectedNodes(),
			treeNode = nodes[0];
			if (nodes.length == 0 || !nodes[0].isParent) {
				alert("请先选择一个节点");
				return;
			}
			zTree.cancelSelectedNode(treeNode);
		};
		function getAllNode() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			node = zTree.getNodes(),
			_nodes = zTree.transformToArray(node);
			for(var i = 0; i < _nodes.length; i++) {
				alert("id = " + _nodes[i].id + "; name = " + _nodes[i].name + "; pId = " + _nodes[i].pId);
			}
		};
		function getChildren(ids,treeNode){
			var _info = {};
			_info.id = treeNode.id;
			_info.name = treeNode.name;
			_info.pId = treeNode.pId;
			ids.push(_info);
			if (treeNode.isParent){  
				for(var obj in treeNode.children){  
					getChildren(ids,treeNode.children[obj]);  
				}  
			}  
	    };
		function submitButtonUpdate() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			node = zTree.getNodes();
			var ids = [];
			for(var i = 0; i < node.length ; i ++) {
				getChildren(ids, node[i]);
			}
			console.log(JSON.stringify(ids));
			StandardPost(
				"${context_ }/${moduleName }/doUpdateView", 
					{_nodes : JSON.stringify(ids),
					dictDictionaryId : $("#id").val(),
					currentPage : $("#currentPage").val(),
					recordPerPage : $("#recordPerPage").val(),
					content : $("#content").val(),
					perfixCode : $("#perfixCode").val(),
					type : $("#type").val()}
			);
		}
		
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			$("#addParent").bind("click", {isParent:true}, add);
			$("#addLeaf").bind("click", {isParent:false}, add);
			$("#edit").bind("click", edit);
			$("#remove").bind("click", remove);
			$("#clearChildren").bind("click", clearChildren);
			$("#clearSelectedNode").bind("click", clearSelectedNode);
			$("#getAllNode").bind("click", getAllNode);
		});
		//-->
	</script>
</body>
</html>
package com.MutiModule.common.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;


/**
 * jdbc操作封装 类名：JDBCWrapperx
 * 
 * 功能：
 * 
 */
public class JDBCSource{
	
	private Connection m_dbcon;
	
	public JDBCSource(String driver,String url,String name,String password,String DataType){
		try {
	        	//加载驱动  
			 	Class.forName(driver);
				//建立连接
				m_dbcon = DriverManager.getConnection(url, name, password);  
			} catch (ClassNotFoundException e) {
				
			} catch (SQLException e) {
				
			}  
	}
	
	/**
	 * 
	 * 功能：<br/> 关闭连接
	 * 
	 */
	public void destroy() {
		if (this.m_dbcon != null) {
			try {
				this.m_dbcon.close();
			} catch (SQLException e) {
				
			}
		}
	}

	// 开启事物
	public boolean beginTransaction() {
		try {
			this.m_dbcon.setAutoCommit(false);
			return true;
		} catch (SQLException e) {
			return false;
		}

	}

	// 提交事务
	public boolean commitTransaction() {
		try {
			this.m_dbcon.commit();
			this.m_dbcon.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			return false;
		}

	}

	// 回滚事物
	public boolean RollbackTransaction() {
		try {
			this.m_dbcon.rollback();
			this.m_dbcon.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * 创建预编译的statement
	 * 
	 * @param sql
	 * @return
	 */
	public PreparedStatement createPreparedStmt(String sql) {
		try {
			return this.m_dbcon.prepareStatement(sql);
		} catch (SQLException e) {
			
		}
		return null;
	}

	/**
	 * 调用存储过程
	 * 
	 * @param procedoreSql
	 * @return
	 */
	public CallableStatement creatCallableStatement(String procedoreSql) {
		try {
			return this.m_dbcon.prepareCall(procedoreSql);
		} catch (SQLException e) {
			
		}
		return null;
	}

	/**
	 * 查询数据库
	 */
	public Object[][] DoQuerry(String sql) {
		Object[][] retArry = new Object[0][0];
		Connection conn = this.m_dbcon;
		if (null == conn)
			return retArry;

		// --构建Statement
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			// --获取结果集架构信息
			ResultSetMetaData rsmeta = rs.getMetaData();
			int iRow = 0;
			int iCol = rsmeta.getColumnCount();
			Vector<Object[]> tmpvt = new Vector<Object[]>();
			// --读结果集
			while (rs.next()) {
				Object[] arTmp = new Object[iCol];
				for (int i = 0; i < iCol; i++) {
					arTmp[i] = rs.getObject(i + 1);
				}
				tmpvt.add(arTmp);
				iRow++;
			}
			retArry = new Object[iRow + 1][iCol];
			// --添加字段名称头，放在第一行
			for (int i = 0; i < iCol; i++) {
				retArry[0][i] = rsmeta.getColumnName(i + 1);
			}
			// --处理存储在集合中的行数组
			Object[] resTmp = tmpvt.toArray();
			System.arraycopy(resTmp, // src
					0, retArry, // dest
					1, iRow);
		} catch (SQLException e) {
			
		} finally {
			try {
				if (null != rs)
					rs.close();
				if (null != stmt)
					stmt.close();
			} catch (SQLException e) {
				
			}

		}
		return retArry;
	}
	
	/**
	 * 查询数据库不带表头
	 */
	public Object[][] Querry(String sql) {
		Object[][] retArry = new Object[0][0];;
		Connection conn = this.m_dbcon;
		if (null == conn)
			return retArry;
		// --构建Statement
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			// --获取结果集架构信息
			ResultSetMetaData rsmeta = rs.getMetaData();
			int iRow = 0;
			int iCol = rsmeta.getColumnCount();
			Vector<Object[]> tmpvt = new Vector<Object[]>();
			// --读结果集
			while (rs.next()) {
				Object[] arTmp = new Object[iCol];
				for (int i = 0; i < iCol; i++) {
					arTmp[i] = rs.getObject(i + 1);
				}
				tmpvt.add(arTmp);
				iRow++;
			}
			retArry = new Object[iRow][iCol];
			Object[] resTmp = tmpvt.toArray();
			System.arraycopy(resTmp,0, retArry, 0, iRow);
		} catch (SQLException e) {
			
		} finally {
			try {
				if (null != rs)
					rs.close();
				if (null != stmt)
					stmt.close();
			} catch (SQLException e) {
				
			}

		}
		return retArry;
	}

	/**
	 * 插入数据 返回类型有void类型修改为boolean类型
	 * 
	 * @param sql
	 * @throws SQLException
	 */
	public boolean insert(String sql) {
		Connection conn = this.m_dbcon;
		if (null == conn) {
			return false;
		}

		// --构建Statement
		Statement stmt = null;
		// 执行语句
		try {
			return conn.createStatement().executeUpdate(sql) > 0;
		} catch (SQLException e) {
			return false;
		} finally {
			try {
				if (null != stmt) {
					stmt.close();
				}
			} catch (SQLException e) {
				
			}
		}
	}

	/**
	 * 修改数据
	 * 
	 * @param sql
	 */
	public boolean update(String sql) {
		if (this.insert(sql)) {
			
			return true;
		} else {
			
			return false;
		}
	}

	/**
	 * 删除数据 返回值类型修改为Boolean
	 * 
	 * @param sql
	 */
	public boolean del(String sql) {
		return this.insert(sql);
	}
}

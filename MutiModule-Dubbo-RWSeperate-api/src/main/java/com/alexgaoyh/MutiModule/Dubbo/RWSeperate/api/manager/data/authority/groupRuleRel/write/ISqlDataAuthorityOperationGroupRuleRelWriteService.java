package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.groupRuleRel.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelKey;

/**
 * SqlDataAuthorityOperationGroupRuleRel 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationGroupRuleRelWriteService {
	
	int deleteByPrimaryKey(SqlDataAuthorityOperationGroupRuleRelKey key);

	int insert(SqlDataAuthorityOperationGroupRuleRelKey record);

	int insertSelective(SqlDataAuthorityOperationGroupRuleRelKey record);

    //alexgaoyh add
    
    /**
     * 根据 groupId 字段标识部分，将 关联关系表的数据删除
     * @param groupKey
     * @return
     */
    int deleteByGroupKey(String groupKey);
}

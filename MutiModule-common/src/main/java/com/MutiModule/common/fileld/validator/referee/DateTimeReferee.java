package com.MutiModule.common.fileld.validator.referee;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.MutiModule.common.fileld.validator.AbstractReferee;
import com.MutiModule.common.fileld.validator.Rule.DateTime;
import com.MutiModule.common.fileld.validator.State;

/**
 * 时间日期 验证器 类名：DateTimeReferee <br />
 *
 * 功能：
 *
 * @author : alexgaoyh <br />
 * @Date : 2017年3月7日 下午1:49:41 <br />
 * @version : 2017年3月7日 <br />
 */
public class DateTimeReferee extends AbstractReferee<DateTime> {
	private final static SimpleDateFormat sdf = new SimpleDateFormat();

	@Override
	public State check(Object data) {
		for (String format : rule.value()) {
			sdf.applyPattern(format);
			try {
				sdf.parse((String) data);
				return simpleSuccess();
			}catch (ParseException e) {
				return failure(getMessageRuleFirst("datetime.format", "The field dose not matches any format"));
			}
		}
		// Impossible
		return simpleSuccess();
	}
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}

}

package com.alexgaoyh.MutiModule.persist.base;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.vo.ZTreeNode;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationMapper;

public class BaseLocationTest {

	private BaseLocationMapper mapper;
	
	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (BaseLocationMapper) ctx.getBean( "baseLocationMapper" );
        
    }
	
	//@Test
	public void test1() {
		
		try {
			List<BaseLocation> entity = mapper.selectTopByParentId();
			System.out.println("entity = " + entity.get(0).getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testZTreeNode() {
		List<ZTreeNode> list = mapper.selectZTreeNodeListByParentId(10000);
		for(ZTreeNode node : list) {
			System.out.println(node);
		}
	}
	
	//@Test
	public void getRegionHashMap() {
		try {
			List<Map<String, Object>> list = mapper.getRegionHashMap();
			
			for(Map<String, Object> map : list) {
				System.out.println(map.get("label") + "|" + map.get("value"));
			}
			
			
			System.out.println(JSONUtilss.toJSon(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.MutiModule.common.utils.dbutils;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;

public class DBUtilsDataSource {
	
	private static Properties properties;  
    private static DataSource dataSource;  
    static {  
        try {  
            properties = new Properties();  
            properties.load(DBUtilsDataSource.class.getResourceAsStream("/jdbc.properties"));  
            BasicDataSourceFactory b = new BasicDataSourceFactory();  
            dataSource = b.createDataSource(properties);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
  
    public static DataSource getdataSource() {  
        return dataSource;  
    }  
    
}
